package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Interests extends Activity implements OnClickListener{
	
	double InterestsRate ;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interests);
		
		Button b1 = (Button)findViewById(R.id.btnConvertR);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.btnSettingR);
		b2.setOnClickListener(this);
		
		TextView showrate = (TextView)findViewById(R.id.showrate);
		InterestsRate = Double.parseDouble(showrate.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interests, menu);
		return true;
	}

	@Override
	public void onClick(View I) {
		// TODO Auto-generated method stub
		int id = I.getId();
		if (id == R.id.btnConvertR) {
			EditText InputMoney = (EditText)findViewById(R.id.InputMoney);
			EditText InputYears = (EditText)findViewById(R.id.InputYears);
			TextView ShowIM = (TextView)findViewById(R.id.ShowIM);
			String res = "";
			try {
				double IR = Float.parseFloat(InputMoney.getText().toString());
				double IY = Float.parseFloat(InputYears.getText().toString());
				double Total = IR * Math.pow(1+(InterestsRate/100),IY);
				res = String.format(Locale.getDefault(), "%.2f", Total);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			ShowIM.setText(res);
		}
		else if (id == R.id.btnSettingR) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("interestRate", InterestsRate);
			startActivityForResult(i, 9999); 
			// 9999 =  a request code, it is a unique integer value for internally identifying
			// return value
		}
	}
	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			InterestsRate = data.getFloatExtra("interestRate", 10.0f);
			TextView ShowRate = (TextView)findViewById(R.id.showrate);
			ShowRate.setText(String.format(Locale.getDefault(), "%.2f", InterestsRate));
		}
	}

}
